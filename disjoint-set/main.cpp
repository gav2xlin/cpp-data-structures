#include <iostream>
#include <vector>
#include <unordered_map>

// https://www.javatpoint.com/disjoint-set-data-structure
// https://www.techiedelight.com/disjoint-set-data-structure-union-find-algorithm/

using namespace std;

class DisjointSet {
    unordered_map<int, int> parent;

public:
    void makeSet(vector<int> const &universe) {
        // create `n` disjoint sets (one for each item)
        for (int i: universe) {
            parent[i] = i;
        }
    }

    int Find(int k) {
        if (parent[k] == k) {
            return k;
        }

        //return Find(parent[k]);
        // merge parent levels to make the flat sets
        parent[k] = Find(parent[k]);
        return parent[k];
    }

    void Union(int a, int b) {
        int x = Find(a);
        int y = Find(b);

        parent[x] = y;
    }
};

void printSets(vector<int> const &universe, DisjointSet &ds) {
    for (int i: universe) {
        cout << ds.Find(i) << " ";
    }
    cout << endl;
}

int main()
{
    vector<int> universe = { 1, 2, 3, 4, 5 };

    DisjointSet ds;

    ds.makeSet(universe);
    printSets(universe, ds);

    ds.Union(4, 3);
    printSets(universe, ds);

    ds.Union(2, 1);
    printSets(universe, ds);

    ds.Union(1, 3);
    printSets(universe, ds);

    // usual: [0]5=5, [1]4=3, [2]3=3, [3]2=1, [4]3=3
    // flatten: [0]5=5, [1]4=3, [2]3=3, [3]2=3, [4]3=3

    return 0;
}
