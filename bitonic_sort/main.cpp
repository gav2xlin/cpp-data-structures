/* This program works only when the size of input is in the power of 2. */
#include <iostream>

// https://www.javatpoint.com/bitonic-sort

using namespace std;

/*In this function the parameter 'd' represents the sorting direction*/
void exchange(int a[], int i, int j, bool d) {
    if (d == (a[i] > a[j])) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}

void merge(int a[], int beg, int c, bool d) {
    if (c > 1) {
        int k = c/2;
        for (int i = beg; i < beg+k; ++i) {
            exchange(a, i, i+k, d);
        }
        merge(a, beg, k, d);
        merge(a, beg+k, k, d);
    }
}

void bitonicSort(int a[],int beg, int c, bool d) {
    if (c > 1) {
        int k = c/2;
        bitonicSort(a, beg, k, true);
        bitonicSort(a, beg+k, k, false);
        merge(a, beg, c, d);
    }
}

void sort(int a[], int n, bool order)
{
    bitonicSort(a, 0, n, order);
}

void printArray(int a[], int n) {
    for(int i = 0; i < n; i++) {
        cout << a[i] << ' ';
    }
}

int main()
{
    int a[]= {20, 60, 30, 70, 50, 10, 5, 40};
    int n = sizeof(a)/sizeof(a[0]);
    bool order = true; //It means sorting in increasing order

    cout << "Before sorting array elements are - \n";
    printArray(a, n);
    sort(a, n, order);

    cout<<"\nAfter sorting array elements are - \n";
    printArray(a, n);
    cout << endl;

    return 0;
}
