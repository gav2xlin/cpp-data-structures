#include <iostream>
#include <algorithm>

// https://www.javatpoint.com/cycle-sort

using namespace std;

void cycleSort(int a[], int n) {
    for (int start = 0; start < n - 1; ++start) {
        int item = a[start];

        int pos = start;
        for (int i = start + 1; i < n; ++i) {
            if (a[i] < item) ++pos;
        }

        if (pos == start) continue;

        while (item == a[pos]) ++pos;

        swap(a[pos], item);

        while (pos != start) {
            pos = start;
            for (int i = start + 1; i < n; ++i) {
                if (a[i] < item) ++pos;
            }

            while (item == a[pos]) ++pos;

            swap(a[pos], item);
        }
    }
}

void printArray(int a[], int n) {
    for (int i = 0; i < n; ++i) {
        cout << a[i] <<" ";
    }
}

int main() {
    int a[] = {89, 44, 29, 19, 9, 39, 59, 49, 4, 1};
    int n = sizeof(a) / sizeof(a[0]);

    cout<<"Before sorting array elements are - \n";
    printArray(a, n);
    cycleSort(a, n);

    cout<<"\nAfter applying cycle sort, array elements are - \n";
    printArray(a, n);
    cout << endl;

    return 0;
}
