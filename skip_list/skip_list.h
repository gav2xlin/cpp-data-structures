#ifndef SKIP_LIST_H
#define SKIP_LIST_H

// https://h-deb.clg.qc.ca/Sujets/Structures-donnees/SkipLists.html
// https://www.javatpoint.com/skip-list-in-data-structure
// https://h-deb.clg.qc.ca/Sources/skip_list.html

#include <optional>
#include <random>
#include <vector>
#include <memory>
#include <ostream>
#include <fstream>
#include <iterator>
#include <utility>

template <class K, class V>
class skip_list {
    class RandomHeight {
        mutable std::mt19937 prng{ std::random_device{}() };
        std::uniform_real_distribution<> cenne{ 0.0, 1.0 };
        int max_level;
        float probability;
    public:
        RandomHeight(int max_level, float probability)
            : max_level{ max_level }, probability{ probability } {
        }

        int new_level() {
            int level = 1;
            while (cenne(prng) < probability && level < max_level) {
                ++level;
            }
            return level;
        }
    };
public:
    using key_type = K;
    using value_type = V;
    using reference = value_type&;
    using const_reference = const value_type&;
    using pointer = value_type*;
    using const_pointer = const value_type*;
    using size_type = std::size_t;
private:
    class node {
        K key_{};
        std::optional<V> value_{};
    public:
        node(K key, const V &value, int height)
            : key_{ key }, value_{ value }, fwd_nodes( height ) {
        }

        node(K key, std::nullptr_t, int height)
            : key_{ key }, fwd_nodes( height ) {
        }

        node(int height) : fwd_nodes( height ) {
        }

        K key() const {
            return key_;
        }

        std::optional<V>& value() {
            return value_;
        }

        std::optional<V> value() const {
            return value_;
        }

        auto height() const {
            return fwd_nodes.size();
        }

        std::vector<node*> fwd_nodes;
    };

    size_type nelems{ };
    node *head;
    node *tail;
    int max_height;
    int cur_height{ 1 };
    RandomHeight randGen;
public:
    skip_list(float probability, int max_height, const K &key)
        : max_height{ max_height }, randGen{max_height, probability} {
        head = new node{ max_height };
        try {
            tail = new node{key, nullptr, max_height};
            for (auto & p : head->fwd_nodes) {
                p = tail;
            }
        } catch (...) {
            delete head;
            throw;
        }
    }

    skip_list(const skip_list&) = delete;

    skip_list& operator=(const skip_list&) = delete;

    skip_list(skip_list &&other)
        : head{ std::exchange(other.head, nullptr) },
        tail{ std::exchange(other.tail, nullptr) },
        max_height{ std::exchange(other.max_height, 0) },
        cur_height{ std::exchange(other.cur_height, 0) },
        nelems{ std::exchange(other.nelems, 0) } {
    }

    skip_list& operator=(skip_list &&other) {
        if (this == &other) return *this;
        head = std::exchange(other.head, nullptr);
        tail = std::exchange(other.tail, nullptr);
        max_height = std::exchange(other.max_height, 0);
        cur_height = std::exchange(other.cur_height, 0);
        nelems = std::exchange(other.nelems, 0);
        return *this;
    }

    ~skip_list() {
        for (auto p = head; p;) {
            auto q = p->fwd_nodes.front();
            delete p;
            p = q;
        }
    }

    bool empty() const noexcept {
        return begin() == end();
    }

    size_type size() const noexcept {
        return nelems;
    }
private:
    node* find_node(std::vector<node*> &update, const K &key) const {
        auto p = head;
        for (auto h = cur_height - 1; h >= 0; --h) {
            for (auto cmpKey = p->fwd_nodes[h]->key();
                 cmpKey < key;
                 cmpKey = p->fwd_nodes[h]->key()) {
                p = p->fwd_nodes[h];
            }
            update[h] = p;
        }
        return p;
    }
public:
    bool insert(const K &key, const V &value) {
        std::vector<node*> update( max_height );

        if (auto p = find_node(update, key);
            p->fwd_nodes.front()->key() == key) {
            return false;
        }

        auto lvl = randGen.new_level();
        if (lvl > cur_height) {
            fill(std::begin(update)+cur_height, std::begin(update)+lvl, head);
            cur_height = lvl;
        }

        auto p = new node{ key, value, lvl };
        for (int i = 0; i < lvl; ++i) {
            p->fwd_nodes[i] = update[i]->fwd_nodes[i];
            update[i]->fwd_nodes[i] = p;
        }

        ++nelems;
        return true;
    }

    bool remove(const K &key) {
        std::vector<node*> update(max_height);

        auto p = find_node(update, key);
        p = p->fwd_nodes.front();
        if (p->key() != key) {
            return false;
        }

        for (int i = 0; i < cur_height && update[i]->fwd_nodes[i] == p; ++i) {
            update[i]->fwd_nodes[i] = p->fwd_nodes[i];
        }
        delete p;

        for (--cur_height;
             cur_height >= 0 && head->fwd_nodes[cur_height]->key() == tail->key(); ) {
            --cur_height;
        }

        --nelems;
        return true;
    }

    std::optional<V> retrieve(const K &key) const {
        std::vector<node*> update(max_height);

        auto p = find_node(update, key);
        p = p->fwd_nodes.front();
        if(p->key() == key) {
            return p->value();
        }

        return {};
    }

    class iterator {
    public:
        using value_type = V;
        using pointer = V *;
        using reference = V &;
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::forward_iterator_tag;
    private:
        node *cur;
        friend class skip_list;
        iterator(node *cur) : cur{ cur } {
        }
    public:
        iterator& operator++() {
            cur = cur->fwd_nodes.front();
            return *this;
        }

        iterator operator++(int) {
            auto temp = *this;
            operator++();
            return temp;
        }

        auto operator*() {
            return cur->value();
        }

        auto operator*() const {
            return cur->value();
        }

        auto operator->() {
            return cur->value();
        }

        auto operator->() const {
            return cur->value();
        }

        bool operator==(const iterator &other) const noexcept {
            return cur == other.cur;
        }

        bool operator!=(const iterator &other) const noexcept {
            return !(*this == other);
        }
    };

    class const_iterator {
    public:
        using value_type = V;
        using pointer = const V *;
        using reference = const V &;
        using difference_type = std::ptrdiff_t;
        using iterator_category = std::forward_iterator_tag;
    private:
        node *cur;
        friend class skip_list;
        const_iterator(node *cur) : cur{ cur } {
        }
    public:
        const_iterator& operator++() {
            cur = cur->fwd_nodes.front();
            return *this;
        }
        const_iterator operator++(int) {
            auto temp = *this;
            operator++();
            return temp;
        }
        auto operator*() const {
            return cur->value();
        }
        auto operator->() const {
            return cur->value();
        }
        bool operator==(const const_iterator &other) const noexcept {
            return cur == other.cur;
        }
        bool operator!=(const const_iterator &other) const noexcept {
            return !(*this == other);
        }
    };

    iterator begin() noexcept {
        return head == tail? tail : head->fwd_nodes.front();
    }

    const_iterator begin() const noexcept {
        return head == tail? tail : head->fwd_nodes.front();
    }

    iterator end() noexcept {
        return tail;
    }

    const_iterator end() const noexcept {
        return tail;
    }

    const_iterator cbegin() const {
        return begin();
    }

    const_iterator cend() const {
        return end();
    }

    friend std::ostream& operator<<(std::ostream &os, const skip_list<K, V> &lst) {
        for (const auto & elem : lst)
            os << *elem << ' ';
        return os;
    }
};

#endif
