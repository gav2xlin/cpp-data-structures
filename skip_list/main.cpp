#include "skip_list.h"
#include <iostream>
#include <algorithm>
#include <string>
#include <string_view>
#include <memory>

using namespace std;

struct Orque {
    float puanteur;
    string nom;
    friend ostream& operator<<(ostream &os, const Orque &orque) {
        return os << orque.nom << ' ' << orque.puanteur;
    }
    Orque(string_view nom, float puanteur) : nom{ nom }, puanteur{ puanteur } {
    }
};

template <class K, class V>
void trouver_et_afficher(const K &key, const skip_list<K, V> &liste) {
    if (auto p = liste.retrieve(key); p)
        cout << "J'ai trouve " << *p << endl;
    else
        cout << key << " introuvable" << endl;
}

int main() {
    skip_list<string, Orque> sList(0.5f, 4, "ZZZZZ"); // clé max arbitraire -- sentinelle
    vector<Orque> v = { { "Urg", 0.2f }, { "Arg", 0.1f }, { "Org", 0.9f } };
    for (auto &orque : v) {
        sList.insert(orque.nom, orque);
    }
    auto cle_test = v[0].nom;
    cout << sList << " ... taille " << sList.size() << endl;
    trouver_et_afficher(cle_test, sList);
    sList.remove(cle_test);
    cout << sList << " ... taille " << sList.size() << endl;
    trouver_et_afficher(cle_test, sList);
    return 0;
}
