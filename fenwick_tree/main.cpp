// https://cp-algorithms.com/data_structures/fenwick.html
// https://medium.com/carpanese/a-visual-introduction-to-fenwick-tree-89b82cac5b3c
// https://robert1003.github.io/2020/01/27/fenwick-tree.html

#include <vector>

using namespace std;

#define lsb(n) (n & (-n))

struct FenwickTree {
    vector<int> v;
    vector<int> fenwick;
    int e = 0; // neutral element

    FenwickTree(vector<int> v) {
        this->v = v;
        fenwick.assign(v.size()+1, e);

        for (int i = 1; i <= v.size(); i++) {
            add(i, v[i-1]);
        }
    }

    int op(int a, int b) {
        return a + b;
    }

    int query(int n) {
        int res = 0;

        while (n > 0) {
            res = op(res, fenwick[n]);
            n -= lsb(n);
        }

        return res;
    }

    int query(int l, int r) {
        l++, r++;

        if (l == 1) return query(r);
        else return query(r) - query(l-1);
    }

    void add(int i, int diff) {
        while (i < fenwick.size()) {
            fenwick[i] += diff;
            i += lsb(i);
        }
    }

    void update(int i, int val) {
        add(i+1, val - v[i]);
        v[i] = val;
    }
};

int main(void) {
    // vector<int> v = {4, 8, 5, 2, 6, 1, 0, 8, 1, 5, 4, 9, 1, 0, 6, 6};
    vector<int> v = {-5, 7, 0, 1, 3, 2, -1, 0, 2};

    FenwickTree bit = FenwickTree(v);

    for (int i = 1; i < bit.fenwick.size(); i++) {
        printf("%d\n", bit.fenwick[i]);
    }

    printf("%d\n", bit.query(1, 4));
    bit.update(3, 5);
    printf("%d\n", bit.query(1, 4));
}
